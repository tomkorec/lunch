<?php

namespace App\Services;

use App\Enum\MealCategory;

class MealCategoryRecognizer
{
    public function __construct()
    {

    }

    public function recognizeByName(string $name): MealCategory
    {
        if ($this->matchesSoup($name)) {
            return MealCategory::SOUP;
        }

        if ($this->matchesSalad($name)) {
            return MealCategory::SALAD;
        }

        return MealCategory::MAIN;
    }

    private function matchesSoup(string $name): bool
    {
        $name = strtolower($name);

        $soupNameVariants = ['krém', 'polévka', 'vývar', 'laksa'];

        $specificSoupNames = ['minestrone', 'miso', 'chowder', 'laksa'];

        $commonSoupNames = ['kulajda', 'cibulačka', 'dršťková', 'česnečka', 'gulášovka', 'gulášová', 'bramboračka', 'bramborová', 'zelňačka'];

        $soupNameVariants = array_merge($soupNameVariants, $specificSoupNames, $commonSoupNames);

        foreach ($soupNameVariants as $soupNameVariant) {
            preg_match('/' . $soupNameVariant . '\s+|$/', $name, $matches);

            if (str_contains($name, $soupNameVariant)) {
                return true;
            }
        }

        return false;
    }

    private function matchesDessert(string $name): bool
    {
        $specificDesertNames = ['lívance'];

        return false;
    }

    private function matchesSalad(string $name): bool
    {
        $saladNames = ['salát', 'šopský', 'caesar'];

        foreach ($saladNames as $saladName) {
            if (str_contains($name, $saladName)) {
                return true;
            }
        }

        return false;
    }
}
