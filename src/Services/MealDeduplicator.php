<?php

namespace App\Services;

use App\Entity\Meal;
use Doctrine\ORM\EntityManagerInterface;

class MealDeduplicator
{
    public function __construct(
        private readonly EntityManagerInterface $em
    )
    {
    }

    // Merge $duplicate into $original
    public function merge(Meal $original, Meal $duplicate): void
    {
        // add $duplicate name as a nameVariant to $original
        $original->addNameVariant($duplicate->getName());

        // add all nameVariants of $duplicate to $original
        foreach ($duplicate->getNameVariants() as $nameVariant) {
            $original->addNameVariant($nameVariant);
        }

        // add all restaurants of the $duplicate to $original
        foreach ($duplicate->getRestaurants() as $restaurant) {
            $original->addRestaurant($restaurant);
        }

        // add all servings of the $duplicate to $original
        foreach ($duplicate->getServings() as $serving) {
            $original->addServing($serving);
        }

        // add all common side dished of the $duplicate to $original
        foreach ($duplicate->getCommonSideDishes() as $commonSideDish) {
            $original->addCommonSideDish($commonSideDish);
        }

        // remove $duplicate from the database
        $this->em->remove($duplicate);

        // flush changes to the database
        $this->em->flush();
    }

    // Return true if $first and $second are duplicates
    public function isDuplicated(Meal $first, Meal $second): bool
    {
        // if the names are the same, they are duplicates
        if ($first->getName() === $second->getName()) {
            return true;
        }

        // if the name is same as any nameVariant of $second, they are duplicates
        if (in_array($second->getName(), $first->getNameVariants(), true)) {
            return true;
        }

        // if they have at leas one nameVariant in common, they are duplicates
        $firstNameVariants = $first->getNameVariants();
        $secondNameVariants = $second->getNameVariants();
        foreach ($firstNameVariants as $firstNameVariant) {
            if (in_array($firstNameVariant, $secondNameVariants, true)) {
                return true;
            }
        }


        // if the names are different, they are not duplicates
        return false;
    }

    // check for possible duplication
    public function mightBeDuplicated(Meal $first, Meal $second): bool
    {

    }

    // get all duplicates
    public function getDuplicates(): array
    {
        $duplicates = [];
        $meals = $this->em->getRepository(Meal::class)->findAll();
        foreach ($meals as $first) {
            foreach ($meals as $second) {
                if ($first === $second) {
                    continue;
                }
                if ($this->isDuplicated($first, $second)) {
                    $duplicates[] = [$first, $second];
                }
            }
        }
        return $duplicates;
    }
}
