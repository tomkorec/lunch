<?php

namespace App\Services\Scraper;

use App\DTO\ServingDTO;
use App\Entity\Meal;
use App\Entity\Restaurant;
use App\Entity\Serving;
use App\Entity\SideDish;
use App\Enum\POS;
use App\Exception\DuplicatedServingException;
use App\Repository\MealRepository;
use App\Repository\ServingRepository;
use App\Repository\SideDishRepository;
use App\Services\Inflector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class BaseScraper
{
    protected static string $name = '';
    protected static string $url = '';
    protected string $code = '';

    /**
     * @var array ServingDTO[]
     */
    protected array $servings = [];

    public function __construct(
        protected EntityManagerInterface $em,
        protected HttpClientInterface    $client,
        protected MealRepository         $mealRepository,
        protected ServingRepository      $servingRepository,
        protected SideDishRepository     $sideDishRepository,
        protected Inflector              $inflector
    )
    {
    }


    protected function findOrCreateSideDishes(array $sideDishesNames): array
    {
        $sideDishesArray = [];

        foreach ($sideDishesNames as $name) {
            $sideDish = $this->sideDishRepository->findSideDishByName($name);

            if ($sideDish === null) {
                $sideDish = new SideDish();
                $sideDish->setName($name);
                $this->em->persist($sideDish);
            }

            $sideDishesArray[] = $sideDish;
        }

        return $sideDishesArray;
    }

    protected function findOrCreateMeal(string $name): Meal
    {
        $meal = $this->mealRepository->findMealByName($name);

        if ($meal === null) {
            $meal = new Meal();
            $meal->setName($name);
            $this->em->persist($meal);
        }

        return $meal;
    }

    /**
     * @throws DuplicatedServingException
     */
    protected function checkExistingServing(Meal $meal, DateTime $date, Restaurant $restaurant): bool
    {
        try {
            $existingServing = $this->servingRepository->findServingByMealAndDateAndRestaurant($meal, $date, $restaurant);
        } catch (NonUniqueResultException) {
            throw new DuplicatedServingException();
        }

        return $existingServing !== null;
    }

    protected function matchCsMonth(string $monthName): int
    {
        return match ($monthName) {
            'ledna' => 1,
            'února' => 2,
            'března' => 3,
            'dubna' => 4,
            'května' => 5,
            'června' => 6,
            'července' => 7,
            'srpna' => 8,
            'září' => 9,
            'října' => 10,
            'listopadu' => 11,
            'prosince' => 12,
        };
    }

    protected function parseMealName(string $fullName): array
    {
        if (preg_match('/(.*)\s+s\s+(.*)/u', $fullName, $matches)) {
            // there are side dishes named after "s"

            //check if there is not a meal with "s" in the name
            $meal = $this->mealRepository->findMealByName($fullName);

            if ($meal) {
                return [$fullName, []];
            }

            $name = trim($matches[1]);


            $sideDishNames = $this->parseSideDishes($matches[2]);

            return [$name, $sideDishNames];
        }

        $parts = preg_split('/,|\s\/\s/', $fullName);
        $name = array_shift($parts);
        $sideDishes = array_map(function (string $dish) {
            return trim($dish);
        }, $parts);

        $name = preg_replace('/^\s+|\s+$/u', '', $name);

        return [$name, $sideDishes];
    }

    protected function parseSideDishes(string $sideDishString): array
    {
        $sideDishNames = [];

        //remove "S" on the beginning if necessary
        $sideDishString = preg_replace('/^[Ss]e?\s+/u', '', $sideDishString);


        $sideDishes = preg_split('/,\s?|\sa\s|\s\/\s/u', trim($sideDishString));
        foreach ($sideDishes as $key => $dish) {
            $words = preg_split('/\s/u', $dish);

            $subject = array_pop($words);

            $subject = $this->inflector->seventh2first($subject, POS::NOUN);

            $sideDishName = '';

            if (!empty($words)) {
                foreach ($words as $word) {
                    $sideDishName .= $this->inflector->seventh2first($word, POS::ADJECTIVE) . ' ';
                }
            }

            $sideDishName .= $subject;

            if (preg_match('/^\s+$/', $sideDishName)) {
                continue;
            }

            $sideDishNames[] = $sideDishName;
        }

        return $sideDishNames;
    }

    protected function saveServings(DateTime $date): void
    {
        /**
         * @var ServingDTO $dto
         */
        foreach ($this->servings as $dto) {
            $meal = $this->findOrCreateMeal($dto->name);
            $restaurant = $this->getRestaurant();

            if ($this->checkExistingServing($meal, $date, $restaurant)) {
                continue;
            }

            $serving = new Serving();
            $serving->setMeal($meal);
            $meal->addRestaurant($restaurant);

            $sideDishes = $this->findOrCreateSideDishes($dto->sideDishes);
            foreach ($sideDishes as $sideDish) {
                $serving->addSideDish($sideDish);
            }

            if (!empty($dto->allergens)) {
                $serving->setAllergies($dto->allergens);
            }

            if ($dto->amount) {
                $serving->setAmount($dto->amount);
            }

            if ($dto->unit) {
                $serving->setAmountUnit($dto->unit);
            }

            $serving->setRestaurant($restaurant);
            $serving->setDate($date);
            $serving->setFullPrice($dto->price);

            $this->em->persist($serving);

        }

        $this->em->flush();
    }

    public function getRestaurant(): Restaurant
    {
        return $this->em->getRepository(Restaurant::class)
            ->findOneBy(['code' => $this->code]);
    }

}
