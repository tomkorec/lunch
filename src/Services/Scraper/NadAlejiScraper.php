<?php

namespace App\Services\Scraper;

use App\DTO\ServingDTO;
use Symfony\Component\DomCrawler\Crawler;

class NadAlejiScraper extends BaseScraper implements ScraperInterface
{
    protected static string $name = 'Nad Alejí';
    protected string $code = 'NAD_ALEJI';
    protected static string $url = 'https://www.nadaleji.cz/';

    public function fetchSite(): string
    {
        $response = $this->client->request('GET', self::$url);

        return $response->getContent();

    }

    public function scrape(): void
    {
        $content = $this->fetchSite();

        $crawler = new Crawler($content);

        $this->assertScrapeable($crawler);

        $date = $this->scrapeDate(clone $crawler);

        $this->scrapeServings(clone $crawler);

        $this->saveServings($date);

    }

    private function assertScrapeable(Crawler $crawler): void
    {
        $assertion = $crawler->filter('#snippet--dayMenu');
        if ($assertion->count() === 0) {
            throw new \Exception('Restaurant is not scrapeable.');
        }
    }

    private function scrapeDate(Crawler $crawler): \DateTime
    {
        $crawler = $crawler->filter('#snippet--dayMenu .day-menu-date');
        $date = $crawler->text();
        $pattern = "/(?<day>\d{1,2})\.(?<month>\d{1,2})/m";
        preg_match($pattern, $date, $matches);

        if (!isset($matches['day'], $matches['month'])) {
            throw new \Exception('Date not found.');
        }

        $day = $matches['day'];
        $month = $matches['month'];

        $year = (int)date('Y');

        return new \DateTime("$year-$month-$day");
    }

    private function scrapeServings(Crawler $crawler): void
    {
        $rows = $crawler->filter('.row.day-menu-wrapper .row.day-menu-food')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        foreach ($rows as $row) {

            preg_match('/^\s*(?:(?<amount>\d+)(?<unit>\w+)\s+)?(?<fullName>\D+)(?<allergens>[\d\,]+)[\s\\n]*(?<price>\d+)/u', $row, $matches);

            $amount = $matches['amount'] ?? null;
            $unit = $matches['unit'] ?? null;
            $fullName = trim($matches['fullName']);
            $allergens = trim($matches['allergens']);
            $price = (int)trim($matches['price']);

            [$name, $sideDishes] = $this->parseMealName($fullName);

            $serving = new ServingDTO();
            $serving->name = $name;
            $serving->sideDishes = $sideDishes;
            $serving->price = $price;
            $serving->amount = $amount ? (int)$amount : null;
            $serving->unit = $unit ?: null;
            $serving->allergens = explode(',', $allergens);

            $this->servings[] = $serving;
        }


    }

}
