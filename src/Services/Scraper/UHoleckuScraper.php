<?php

namespace App\Services\Scraper;

use App\DTO\ServingDTO;
use App\Entity\Serving;
use Symfony\Component\DomCrawler\Crawler;

class UHoleckuScraper extends BaseScraper implements ScraperInterface
{
    protected static string $name = 'U Holečků';
    public string $code = 'U_HOLECKU';
    protected static string $url = 'https://www.uholecku.cz/denni-menu/';

    public function scrape(): void
    {
        $content = $this->fetchSite();

        $crawler = new Crawler($content);

        //first assert that the restaurant is scrapeable
        $this->assertScrapeable($crawler);

        //then scrape the date
        $date = $this->scrapeDate($crawler);

        //then fill up the servings array
        $this->scrapeServings(clone $crawler);

        //then save the servings to the database
        $this->saveServings($date);
    }

    public function fetchSite(): string
    {
        $response = $this->client->request('GET', self::$url);

        //todo: check if response is ok

        return $response->getContent();
    }

    private function assertScrapeable(Crawler $crawler): void
    {
        $assertion = $crawler->filter('.hentry__content');
        if ($assertion->count() === 0) {
            throw new \Exception('Restaurant is not scrapeable.');
        }
    }

    private function parseRow(Crawler $row): array|false
    {
        if ((clone $row)->filter('td')->count() === 0) {
            return false;
        }

        $name = '';

        $nameCrawl = (clone $row)->filter('td:not([style])');
        if ($nameCrawl->count() > 0) {
            $name = $nameCrawl->text();
        }

        $price = '';
        $priceCrawl = (clone $row)->filter('td[style="text-align: right;"]');
        if ($priceCrawl->count() > 0) {
            $price = $priceCrawl->text();
        }

        if ($name === '' || $price === '') {
            return false;
        }

        return [$name, $price];
    }

    /**
     * @param Crawler $crawler
     * @return void
     * As a result, we should fill the servings array.
     */
    private function scrapeServings(Crawler $crawler): void
    {
        $rows = $crawler->filter('table tr');

        $rows->each(function (Crawler $row) {
            $serving = new ServingDTO();
            $parsedRow = $this->parseRow(clone $row);

            if ($parsedRow === false) {
                return;
            }

            [$name, $price] = $parsedRow;

            $serving->name = $name;
            $serving->price = $price;

            $this->servings[] = $serving;

        });
    }

    private function scrapeDate(Crawler $param): \DateTime
    {
        $date = $param->filter('.hentry__content h1')->text();
        $date = trim($date);
        preg_match('/\S+\s+(\d+)\.\s+(\w+)/u', $date, $matches);

        $month = $this->matchCsMonth($matches[2]);

        $dayOfMonth = $matches[1];

        $currentYear = date('Y');

        return new \DateTime($currentYear . '-' . $month . '-' . $dayOfMonth);
    }

    protected function saveServings(\DateTime $date): void
    {
        foreach ($this->servings as $dto) {
            $serving = new Serving();

            [$mealName, $sideDishesNames, $allergies] = $this->parseMealName($dto->name);

            if ($mealName === '') {
                continue;
            }

            $sideDishes = $this->findOrCreateSideDishes($sideDishesNames);


            $meal = $this->findOrCreateMeal($mealName);
            $restaurant = $this->getRestaurant();

            $serving->setAllergies($allergies);

            if ($this->checkExistingServing($meal, $date, $restaurant)) {
                continue;
            }

            $serving->setMeal($meal);
            $meal->addRestaurant($restaurant);

            foreach ($sideDishes as $dish) {
                $serving->addSideDish($dish);
            }

            // match number in price
            $price = $this->matchPrice($dto->price);

            $serving->setFullPrice($price);
            $serving->setRestaurant($restaurant);

            $serving->setDate($date);
            $this->em->persist($serving);
        }

        $this->em->flush();
    }

    protected function parseMealName($name): array
    {
        $name = trim($name);

        preg_match('/([\-–\w .]+(?:\([^)]+\))?),?(\D*)([\d,]+)\s*$/u', $name, $matches);

        if (count($matches) === 0) {
            return [$name, [], []];
        }

        $name = $matches[1];


        if (preg_match('/\S+/u', $matches[2])) {
            $sideDishes = explode(',', $matches[2]);
        } else {
            $sideDishes = [];
        }

        if (preg_match('/^\d+/u', $matches[3])) {
            $allergies = explode(',', $matches[3]);
        } else {
            $allergies = [];
        }
        return [$name, $sideDishes, $allergies];
    }

    private function matchPrice(string $price): null|int
    {
        preg_match('/(\d+)/u', $price, $matches);
        return $matches[1] ?? null;
    }

}
