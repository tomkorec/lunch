<?php

namespace App\Services\Scraper;

use App\DTO\ServingDTO;
use DateTime;
use Smalot\PdfParser\Parser;
use Symfony\Component\DomCrawler\Crawler;

class LibockyDvurScraper extends BaseScraper implements ScraperInterface
{
    protected static string $name = 'Libocky Dvůr';
    protected string $code = 'LIBOCKY_DVUR';
    protected static string $url = 'https://libocky-dvur.eatbu.com/?lang=cs';

    public function scrape(): void
    {
        $content = $this->fetchSite();

        $date = $this->scrapeDate($content);

        $this->scrapeServings($content);

        $this->saveServings($date);

    }

    public function fetchSite(): string
    {
        $menuUrl = 'https://cdn.website.dish.co/media/06/80/8564869/Poledni-menu.pdf';


        $parser = new Parser();
        $pdf = $parser->parseFile($menuUrl);

        return $pdf->getText();
    }

    private function scrapeServings(string $content): void
    {
        //replace all tab characters with spaces
        $content = preg_replace('/\\t+/', '', $content);

        preg_match_all('/([\w\s,.]*(?:\\n)?[\w\s,.]+)\s+ (\d+),-/mu', $content, $matches);

        foreach ($matches[1] as $key => $fullName) {
            $fullName = preg_replace(
                ['/\\n/', '/Specialita na tento týden/u', '/\s+/', '/^\d+/'],
                ['', '', ' ', ''],
                $fullName);
            $fullName = trim($fullName);

            [$mealName, $sideDishes] = $this->parseMealName($fullName);

            if (!$mealName) {
                continue;
            }


            $price = trim($matches[2][$key]);

            $servingDTO = new ServingDTO();
            $servingDTO->sideDishes = $sideDishes;
            $servingDTO->name = $mealName;
            $servingDTO->price = $price;

            $this->servings[] = $servingDTO;
        }
    }

    private function scrapeDate(string $content): DateTime
    {
        $content = preg_replace('/\t|\/t/', '', $content);
        $content = preg_replace('/\s+/', '', $content);
        preg_match('/PoledníMenu(\d{1,2})\.\s*(\d{1,2})\.\s*(\d{4})/u', $content, $matches);

        return new DateTime($matches[3] . '-' . $matches[2] . '-' . $matches[1]);
    }

    private function getMenuUrl(): string
    {
        $response = $this->client->request('GET', self::$url)->getContent();

        $crawler = new Crawler($response);

        return $crawler->filter('.menu-downloads a')->last()->attr('href');


    }
}
