<?php

namespace App\Services\Scraper;

interface ScraperInterface
{

    public function fetchSite(): string;

    public function scrape(): void;

}
