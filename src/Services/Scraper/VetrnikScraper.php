<?php

namespace App\Services\Scraper;

use App\DTO\ServingDTO;
use Symfony\Component\DomCrawler\Crawler;

class VetrnikScraper extends BaseScraper implements ScraperInterface
{
    protected static string $name = 'Větrník';
    protected string $code = 'VETRNIK';
    protected static string $url = 'https://www.restauracevetrnik.cz/poledni-menu/';

    public function scrape(): void
    {
        $content = $this->fetchSite();

        $crawler = new Crawler($content);

        $this->assertScrapeable($crawler);

        $day = $crawler->filter('.elementor-column > .elementor-widget-wrap.elementor-element-populated  .elementor-column.elementor-col-50');

        $day->each(function (Crawler $row) {
            $this->servings = [];

            if ($row->filter('.jet-price-list')->count() === 0) {
                return;
            }

            $date = $this->scrapeDate($row);

            $this->scrapeServings(clone $row);

            $this->saveServings($date);
        });

    }

    public function fetchSite(): string
    {
        $response = $this->client->request('GET', self::$url);

        return $response->getContent();
    }

    private function assertScrapeable(Crawler $crawler): void
    {
        $assertion = $crawler->filter('.elementor-column > .elementor-widget-wrap.elementor-element-populated');
        if ($assertion->count() === 0) {
            throw new \Exception('Restaurant is not scrapeable.');
        }
    }

    private function scrapeDate(Crawler $crawler): \DateTime
    {
        $crawler = $crawler->filter('h3.raven-heading');


        $text = $crawler->text();
        preg_match('/(\d+)\.\s+(\d+)\.\s+(\d+)/', $text, $matches);

        [$match, $day, $month, $year] = $matches;

        return new \DateTime("$year-$month-$day");
    }

    private function scrapeServings(Crawler $crawler)
    {


        $crawler->filter('.elementor-jet-price-list .price-list__item')
            ->each(function (Crawler $crawler) {
                $serving = new ServingDTO();

                $name = $crawler->filter('h5')->text();
                $price = $crawler->filter('.price-list__item-price')->text();

                if ($name === '' || $price === '') {
                    return;
                }

                preg_match('(\d+)', $price, $matches);


                $price = $matches[0];


                $details = $crawler->filter('.price-list__item-desc')->text();

                preg_match('/(?:(\d+)\s+(\w+)\s+\|\s+)?([^\(]*)(?:\(([^\)]+)\))*$/u', $details, $matches);

                $amount = $matches[1] ?? null;
                $unit = $matches[2] ?? null;
                $sideDishes = $matches[3] ?? null;
                $allergies = $matches[4] ?? null;

                $allergies = explode(', ', $allergies);
                $serving->allergens = $allergies;

                $serving->sideDishes = $sideDishes ? $this->parseSideDishes($sideDishes) : [];

                $serving->name = $name;
                $serving->price = $price;

                if ($amount) {
                    $serving->amount = (int)$amount;
                }

                if ($unit) {
                    $serving->unit = $unit;
                }


                $this->servings[] = $serving;

            });

    }

}
