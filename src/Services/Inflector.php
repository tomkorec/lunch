<?php

namespace App\Services;

use App\Enum\POS;

class Inflector
{
    public function seventh2first(string $word, POS $pos): string
    {
        $patterns = [];
        $replacements = [];

        if ($pos === POS::NOUN) {
            $patterns = [
                '/(\w+)em$/u',
                '/(\w+)ami$/u',
                '/(\w+)(ou)$/u',
                '/(\w+)ší$/u',
            ];
            $replacements = [
                '$1',
                '$1y',
                '$1a',
                '$1še'
            ];
        }

        if ($pos === POS::ADJECTIVE) {
            $patterns = [
                '/(\w+)(ou)$/u',
                '/(\w+)ím$/u',
                '/(\w+)ým?$/u',
                '/(\w+)ými?$/u',
            ];
            $replacements = [
                '$1á',
                '$1í',
                '$1ý',
                '$1é'
            ];
        }


        return preg_replace($patterns, $replacements, $word);
    }
}
