<?php

namespace App\Exception;

use Exception;

class DuplicatedServingException extends Exception
{
    protected $message = 'The restaurant has two servings for the same meal at the same time.';
}
