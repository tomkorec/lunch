<?php

namespace App\Command;

use App\Entity\Restaurant;
use App\Entity\ScraperExecution;
use App\Services\Scraper\ScraperInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand(
    name: 'app:scrape',
    description: 'Scrapes servings from a restaurant website.',
)]
class ScrapeCommand extends Command
{
    public function __construct(
        private readonly iterable               $scrapers,
        private readonly EntityManagerInterface $em,
        private readonly TranslatorInterface    $translator
    )
    {
        parent::__construct($this->getName());
    }

    protected function configure(): void
    {
        $scrapeableRestaurants = $this->em->getRepository(Restaurant::class)->findBy(['scrapeable' => true]);

        $this
            ->addOption(
                'restaurant',
                'r',
                InputOption::VALUE_NONE,
                'Scrape specific restaurant only: user selection'
            )
            ->addOption(
                'code',
                'c',
                InputOption::VALUE_REQUIRED,
                'Choose single restaurant by code'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        $selectedRestaurant = null;

        if ($input->getOption('restaurant')) {
            $scrapeableRestaurants = $this->em->getRepository(Restaurant::class)->findBy(['scrapeable' => true]);

            $question = new ChoiceQuestion('Zvolte restauraci', $scrapeableRestaurants);
            $selectedRestaurant = $io->askQuestion($question);
        }

        if ($code = $input->getOption('code')) {
            $selectedRestaurant = $this->em->getRepository(Restaurant::class)->findOneBy(['scrapeable' => true, 'code' => $code]);
        }

        /** @var ScraperInterface $scraper */
        foreach ($this->scrapers as $scraper) {

            /** @var Restaurant $restaurant */
            $restaurant = $scraper->getRestaurant();

            if ($selectedRestaurant && $restaurant->getId() !== $selectedRestaurant->getId()) {
                continue;
            }

            if (!$restaurant->isScrapeable()) {
                continue;
            }

            $io->title($this->translator->trans('scraper.scraping', ['%restaurant%' => $restaurant->getName()]));

            $execution = new ScraperExecution();
            $execution->setExecutionTime(new DateTimeImmutable());

            try {
                $scraper->scrape();
                $execution->setSuccess(true);
            } catch (\Exception $e) {
                $io->error($e->getMessage());
                $execution->setSuccess(false);
            } finally {
                $execution->setRestaurant($restaurant);
                $this->em->persist($execution);
                $this->em->flush();
            }
        }

        $io->success($this->translator->trans('scraper.finished'));

        return Command::SUCCESS;
    }

    private function getScrapeableRestaurants(): array
    {
        $restaurants = $this->em->getRepository(Restaurant::class)->findBy([
            'scrapeable' => true,
        ]);

        $f = function (int $k, Restaurant $v) {
            return [$v->getCode(), $v->getName()];
        };

        return array_column(array_map($f, array_keys($restaurants), $restaurants), 1, 0);;
    }
}
