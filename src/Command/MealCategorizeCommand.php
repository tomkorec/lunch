<?php

namespace App\Command;

use App\Repository\MealCategoryRepository;
use App\Repository\MealRepository;
use App\Services\MealCategoryRecognizer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:meal:categorize',
    description: 'Categorizes meals by their name',
)]
class MealCategorizeCommand extends Command
{
    public function __construct(
        private readonly MealRepository         $mealRepository,
        private readonly MealCategoryRepository $categoryRepository,
        private readonly MealCategoryRecognizer $recognizer,
        private readonly EntityManagerInterface $em
    )
    {
        parent::__construct($this->getName());
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $uncategorizedMeals = $this->mealRepository->matching(
            Criteria::create()->where(Criteria::expr()->isNull('category'))
        );

        $io->title('Categorizing meals');

        foreach ($uncategorizedMeals as $meal) {
            $result = $this->recognizer->recognizeByName($meal);

            $category = $this->categoryRepository->findOneBy(['id' => $result->value]);

            if (!$category) {
                $io->error(sprintf('Could not find category with id %s', $result->value));
                continue;
            }

            $meal->setCategory($category);

            $io->info(sprintf('%s -> %s', $meal->getName(), $category->getName()));
        }

        $this->em->flush();
        
        $io->success('Meals were successfully categorized');


        return Command::SUCCESS;
    }
}
