<?php

namespace App\Enum;

/**
 * Part of speech enum.
 */
enum POS
{
    case NOUN;
    case ADJECTIVE;
}
