<?php

namespace App\Enum;

enum ScrapeStatus: string
{
    case UP_TO_DATE = 'up-to-date';
    case OUT_OF_DATE = 'out-of-date';
    case NOT_SCRAPED = 'not-scraped-yet';
    case FAILED = 'failed';
    case NOT_SCRAPEABLE = 'not-scrapeable';
}
