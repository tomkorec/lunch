<?php

namespace App\Enum;

enum MealCategory: int
{
    case SOUP = 1;
    case MAIN = 2;
    case DESSERT = 3;
    case SALAD = 4;
}
