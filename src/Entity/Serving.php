<?php

namespace App\Entity;

use App\Repository\ServingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServingRepository::class)]
class Serving
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'servings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Meal $meal = null;

    #[ORM\ManyToOne(inversedBy: 'servings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Restaurant $restaurant = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\ManyToMany(targetEntity: SideDish::class, inversedBy: 'servings')]
    private Collection $sideDish;

    #[ORM\Column(nullable: true)]
    private ?int $price = null;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private ?array $allergies = [];

    #[ORM\Column(nullable: true)]
    private ?int $amount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $amountUnit = null;

    public function __construct()
    {
        $this->sideDish = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMeal(): ?Meal
    {
        return $this->meal;
    }

    public function setMeal(?Meal $meal): self
    {
        $this->meal = $meal;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, SideDish>
     */
    public function getSideDish(): Collection
    {
        return $this->sideDish;
    }

    public function addSideDish(SideDish $sideDish): self
    {
        if (!$this->sideDish->contains($sideDish)) {
            $this->sideDish->add($sideDish);
        }

        return $this;
    }

    public function removeSideDish(SideDish $sideDish): self
    {
        $this->sideDish->removeElement($sideDish);

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceAsString(bool $currency = false): string
    {
        $price = $this->getFullPrice();
        $priceString = number_format($price, 0, ',', ' ') . ($currency ?
                (floor($price) === $price ? ',-' : '') .
                ' Kč' : '');
        return $price ? $priceString : '';
    }

    public function getPriceInCrowns(): string
    {
        return $this->getPriceAsString(true);
    }

    public function getFullPrice(): float
    {
        return $this->price / 100;
    }

    public function setFullPrice(float|int $price): self
    {
        $this->price = $price * 100;
        return $this;
    }

    public function getAllergies(): array
    {
        if (!$this->allergies) {
            return [];
        }

        return $this->allergies;
    }

    public function setAllergies(array $allergies): self
    {
        $this->allergies = $allergies;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmountUnit(): ?string
    {
        return $this->amountUnit;
    }

    public function setAmountUnit(?string $amountUnit): self
    {
        $this->amountUnit = $amountUnit;

        return $this;
    }

    public function getAmountWithUnit(): string
    {
        if (!$this->amount) {
            return '1';
        }

        return $this->amount . ' ' . $this->amountUnit;
    }

    public function getFullName()
    {
        $name = $this->getMeal()?->getName();

        foreach ($this->getSideDish() as $sideDish) {
            $name .= ', ' . $sideDish->getName();
        }

        return $name;
    }
}
