<?php

namespace App\Entity;

use App\Enum\ScrapeStatus;
use App\Repository\RestaurantRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RestaurantRepository::class)]
class Restaurant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: Serving::class)]
    private Collection $servings;

    #[ORM\ManyToMany(targetEntity: Meal::class, mappedBy: 'restaurants')]
    private Collection $meals;

    #[ORM\Column]
    private ?bool $scrapeable = null;

    #[ORM\Column]
    private ?string $code = null;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: ScraperExecution::class)]
    private Collection $scraperExecutions;

    public function __construct()
    {
        $this->servings = new ArrayCollection();
        $this->meals = new ArrayCollection();
        $this->scraperExecutions = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection<int, Serving>
     */
    public function getServings(): Collection
    {
        return $this->servings;
    }

    public function addServing(Serving $serving): self
    {
        if (!$this->servings->contains($serving)) {
            $this->servings->add($serving);
            $serving->setRestaurant($this);
        }

        return $this;
    }

    public function removeServing(Serving $serving): self
    {
        if ($this->servings->removeElement($serving)) {
            // set the owning side to null (unless already changed)
            if ($serving->getRestaurant() === $this) {
                $serving->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Meal>
     */
    public function getMeals(): Collection
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal): self
    {
        if (!$this->meals->contains($meal)) {
            $this->meals->add($meal);
            $meal->addRestaurant($this);
        }

        return $this;
    }

    public function removeMeal(Meal $meal): self
    {
        if ($this->meals->removeElement($meal)) {
            $meal->removeRestaurant($this);
        }

        return $this;
    }

    public function isScrapeable(): ?bool
    {
        return $this->scrapeable;
    }

    public function setScrapeable(bool $scrapeable): self
    {
        $this->scrapeable = $scrapeable;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;
        return $this;
    }


    private function getCodeFromName(): string
    {
        $code = \Transliterator::create('Any-Latin; Latin-ASCII; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();')->transliterate($this->name);

        //convert code to uppercase
        $code = \mb_strtoupper($code);

        //replace all spaces with underscore
        $code = \preg_replace('/\s+/', '_', $code);

        return $code;
    }

    /**
     * @return Collection<int, ScraperExecution>
     */
    public function getScraperExecutions(): Collection
    {
        return $this->scraperExecutions;
    }

    public function addScraperExecution(ScraperExecution $scraperExecution): self
    {
        if (!$this->scraperExecutions->contains($scraperExecution)) {
            $this->scraperExecutions->add($scraperExecution);
            $scraperExecution->setRestaurant($this);
        }

        return $this;
    }

    public function removeScraperExecution(ScraperExecution $scraperExecution): self
    {
        if ($this->scraperExecutions->removeElement($scraperExecution)) {
            // set the owning side to null (unless already changed)
            if ($scraperExecution->getRestaurant() === $this) {
                $scraperExecution->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getLastScrapedAt(): DateTimeImmutable|null
    {
        if ($this->scraperExecutions->isEmpty()) {
            return null;
        }

        return $this->scraperExecutions->first()->getExecutionTime();
    }

    public function isScrapedToday()
    {
        if ($this->scraperExecutions->isEmpty()) {
            return false;
        }

        $lastScrapedAt = $this->scraperExecutions->last()->getExecutionTime();
        return $lastScrapedAt->format('Y-m-d') === date('Y-m-d');
    }

    public function isNotWorking()
    {
        if ($this->scraperExecutions->isEmpty()) {
            return false;
        }

        $lastExecution = $this->scraperExecutions->last();

        return $lastExecution->isSuccess() === false;
    }

    public function getScraperState(): ScrapeStatus
    {
        if (!$this->scrapeable) {
            return ScrapeStatus::NOT_SCRAPEABLE;
        }

        if ($this->scraperExecutions->isEmpty()) {
            return ScrapeStatus::NOT_SCRAPED;
        }

        $lastExecution = $this->scraperExecutions->last();

        if (!$lastExecution->isSuccess()) {
            return ScrapeStatus::FAILED;
        }

        if ($this->isScrapedToday()) {
            return ScrapeStatus::UP_TO_DATE;
        }

        return ScrapeStatus::OUT_OF_DATE;
    }

    public function getTodayServings(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('date', new DateTimeImmutable('today')))
            ->orderBy(['date' => 'ASC']);


        return $this->servings->matching($criteria);
    }

    public function hasServingsForToday(): bool
    {
        return $this->getTodayServings()->count() > 0;
    }
}
