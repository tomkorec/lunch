<?php

namespace App\Entity;

use App\Repository\ScraperExecutionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ScraperExecutionRepository::class)]
class ScraperExecution
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'scraperExecutions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Restaurant $restaurant = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $executionTime = null;

    #[ORM\Column]
    private bool $success = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getExecutionTime(): ?\DateTimeImmutable
    {
        return $this->executionTime;
    }

    public function setExecutionTime(\DateTimeImmutable $executionTime): self
    {
        $this->executionTime = $executionTime;

        return $this;
    }

    public function isSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }
}
