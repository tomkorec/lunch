<?php

namespace App\Entity;

use App\Repository\MealRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MealRepository::class)]
class Meal
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $vegetarian = false;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array $nameVariants = [];

    #[ORM\OneToMany(mappedBy: 'meal', targetEntity: Serving::class)]
    private Collection $servings;

    #[ORM\ManyToMany(targetEntity: Restaurant::class, inversedBy: 'meals')]
    private Collection $restaurants;

    #[ORM\ManyToMany(targetEntity: SideDish::class, inversedBy: 'commonMeals')]
    private Collection $commonSideDishes;

    #[ORM\ManyToOne(inversedBy: 'meal')]
    private ?MealCategory $category = null;

    public function __construct()
    {
        $this->restaurants = new ArrayCollection();
        $this->commonSideDishes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameVariants(): array
    {
        return $this->nameVariants;
    }

    public function setNameVariants(?array $nameVariants): self
    {
        $this->nameVariants = $nameVariants;

        return $this;
    }

    public function setVegetarian(bool $vegetarian): self
    {
        $this->vegetarian = $vegetarian;
        return $this;
    }

    public function isVegetarian(): bool
    {
        return $this->vegetarian;
    }

    public function addNameVariant(string $nameVariant): self
    {
        if (in_array($nameVariant, $this->nameVariants, true)) {
            return $this;
        }

        $this->nameVariants[] = $nameVariant;
        return $this;
    }

    public function getAllNames(): array
    {
        return array_merge([$this->name], $this->nameVariants);
    }

    /**
     * @return Collection<int, Serving>
     */
    public function getServings(): Collection
    {
        return $this->servings;
    }

    public function addServing(Serving $serving): self
    {
        if (!$this->servings->contains($serving)) {
            $this->servings->add($serving);
            $serving->setMeal($this);
        }

        return $this;
    }

    public function removeServing(Serving $serving): self
    {
        if ($this->servings->removeElement($serving)) {
            // set the owning side to null (unless already changed)
            if ($serving->getMeal() === $this) {
                $serving->setMeal(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Restaurant>
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants->add($restaurant);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        $this->restaurants->removeElement($restaurant);

        return $this;
    }

    /**
     * @return Collection<int, SideDish>
     */
    public function getCommonSideDishes(): Collection
    {
        return $this->commonSideDishes;
    }

    public function addCommonSideDish(SideDish $commonSideDish): self
    {
        if (!$this->commonSideDishes->contains($commonSideDish)) {
            $this->commonSideDishes->add($commonSideDish);
        }

        return $this;
    }

    public function removeCommonSideDish(SideDish $commonSideDish): self
    {
        $this->commonSideDishes->removeElement($commonSideDish);

        return $this;
    }

    public function getCategory(): ?MealCategory
    {
        return $this->category;
    }

    public function setCategory(?MealCategory $category): self
    {
        $this->category = $category;

        return $this;
    }


}
