<?php

namespace App\Entity;

use App\Repository\SideDishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SideDishRepository::class)]
class SideDish
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\ManyToMany(targetEntity: Serving::class, mappedBy: 'sideDish')]
    private Collection $servings;

    #[ORM\ManyToMany(targetEntity: Meal::class, mappedBy: 'commonSideDishes')]
    private Collection $commonMeals;

    public function __construct()
    {
        $this->servings = new ArrayCollection();
        $this->commonMeals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Serving>
     */
    public function getServings(): Collection
    {
        return $this->servings;
    }

    public function addServing(Serving $serving): self
    {
        if (!$this->servings->contains($serving)) {
            $this->servings->add($serving);
            $serving->addSideDish($this);
        }

        return $this;
    }

    public function removeServing(Serving $serving): self
    {
        if ($this->servings->removeElement($serving)) {
            $serving->removeSideDish($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Meal>
     */
    public function getCommonMeals(): Collection
    {
        return $this->commonMeals;
    }

    public function addCommonMeal(Meal $commonMeal): self
    {
        if (!$this->commonMeals->contains($commonMeal)) {
            $this->commonMeals->add($commonMeal);
            $commonMeal->addCommonSideDish($this);
        }

        return $this;
    }

    public function removeCommonMeal(Meal $commonMeal): self
    {
        if ($this->commonMeals->removeElement($commonMeal)) {
            $commonMeal->removeCommonSideDish($this);
        }

        return $this;
    }
}
