<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Entity\Serving;
use App\Repository\MealCategoryRepository;
use App\Repository\RestaurantRepository;
use App\Repository\ServingRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomepageController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(Request $request, ServingRepository $servingRepository): Response
    {
        $qb = $servingRepository->findServingsForToday();

        $servings = new Pagerfanta(
            new QueryAdapter($qb)
        );

        $servings->setCurrentPage($request->query->getInt('page', 1));

        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'servings' => $servings
        ]);
    }

    #[Route('/random', name: 'app_random_serving')]
    public function random(Request $request, ServingRepository $servingRepository, MealCategoryRepository $categoryRepository): Response
    {
        $form = $this->createFormBuilder()
            ->add('restaurant', EntityType::class, [
                'class' => Restaurant::class,
                'choice_label' => 'name',
                'placeholder' => 'Zvolte restauraci',
                'query_builder' => static function (RestaurantRepository $repository) {
                    return $repository->createQueryBuilder('r')
                        ->where('r.scrapeable = true')
                        ->orderBy('r.name', 'ASC');
                },
                'required' => false,
                'label' => false
            ])
            ->add('noSalad', CheckboxType::class, [
                'label' => 'Nechci salát',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Vybrat',
                'attr' => [
                    'class' => 'button light wide',
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        $serving = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $restaurant = $form->getData()['restaurant'];
            $noSalad = $form->getData()['noSalad'];


            $servings = $servingRepository->findServingsForToday($restaurant)
                ->getQuery()
                ->getResult();

            if ($noSalad) {
                $salad = $categoryRepository->find(4);

                $servings = array_filter($servings, static function (Serving $serving) use ($salad) {
                    return !$serving->getMeal()->getCategory() !== $salad;
                });
            }


            if (count($servings) === 0) {
                $this->addFlash('warning', 'Dnes není v restauraci žádné takové jídlo.');
            } else {
                $serving = $servings[array_rand($servings)];
            }

        }

        return $this->render('homepage/random.html.twig', [
            'form' => $form->createView(),
            'serving' => $serving
        ]);
    }
}
