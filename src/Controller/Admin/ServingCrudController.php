<?php

namespace App\Controller\Admin;

use App\Entity\Serving;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ServingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Serving::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Porce')
            ->setEntityLabelInPlural('Porce');

    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }


    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('restaurant')
            ->add('date');
    }

    public function configureFields(string $pageName): iterable
    {

        yield DateField::new('date', 'Datum');
        yield AssociationField::new('restaurant', 'Restaurace');
        yield AssociationField::new('meal', 'Jídlo');

        yield TextField::new('priceInCrowns', 'Cena')
            ->onlyOnIndex();
        yield IntegerField::new('fullPrice', 'Cena')
            ->onlyOnForms();

        yield AssociationField::new('sideDish', 'Příloha');
        yield ArrayField::new('allergies', 'Alergie');
        yield TextField::new('amountWithUnit', 'Množství')
            ->onlyOnDetail();
    }

}
