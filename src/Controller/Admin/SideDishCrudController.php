<?php

namespace App\Controller\Admin;

use App\Entity\SideDish;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SideDishCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SideDish::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
