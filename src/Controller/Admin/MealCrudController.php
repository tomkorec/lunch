<?php

namespace App\Controller\Admin;

use App\Entity\Meal;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MealCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Meal::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name', 'Název jídla');
        yield AssociationField::new('category', 'Kategorie');
        yield BooleanField::new('vegetarian', 'Vegetariánské');
        yield AssociationField::new('commonSideDishes', 'Obvyklé přílohy');
        yield AssociationField::new('restaurants', 'Restaurace')
            ->setHelp('Restaurace, které jsou tímto jídlem vyhlášeny');

        yield ArrayField::new('nameVariants', 'Varianty názvu')
            ->setHelp('Varianty názvu jídla, které se mohou vyskytnout v jídelních lístkách')
            ->hideOnIndex();
    }

}
