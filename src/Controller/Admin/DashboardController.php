<?php

namespace App\Controller\Admin;

use App\Entity\Meal;
use App\Entity\Restaurant;
use App\Entity\Serving;
use App\Entity\SideDish;
use App\Repository\RestaurantRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Attribute\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private RestaurantRepository $restaurantRepository
    ) {}

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $restaurants = $this->restaurantRepository->findAll();


        return $this->render('admin/dashboard/index.html.twig', [
            'restaurants' => $restaurants,
        ]);
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addWebpackEncoreEntry('admin');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Obědy');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Přehled', 'fa fa-tachometer-alt');
        yield MenuItem::section('Pokrmy');
        yield MenuItem::linkToCrud('Jídlo', 'fa fa-drumstick-bite', Meal::class);
        yield MenuItem::linkToCrud('Přílohy', 'fa fa-bowl-rice', SideDish::class);

        yield MenuItem::section('Podniky');
        yield MenuItem::linkToCrud('Restaurace', 'fa fa-cutlery', Restaurant::class);
        yield MenuItem::linkToCrud('Porce', 'fa fa-utensil-spoon', Serving::class);
    }

    #[Route('/admin/scrape', name: 'admin_scrape', methods: ['POST'])]
    public function scrape(Request $request, KernelInterface $kernel): Response
    {
        $renderCards = $request->request->getBoolean('render_cards');
        $code = $request->request->getString('code');


        $application = new Application($kernel);
        $application->setAutoExit(false);

        $inputArray = [
            'command' => 'app:scrape',
        ];

        if ($code) {
            $inputArray['--code'] = $code;
        }

        $input = new ArrayInput($inputArray);

        $output = new BufferedOutput();

        $application->run($input, $output);


        if ($renderCards) {
            return $this->render('admin/dashboard/@partials/_restaurant_cards.html.twig', [
                'restaurants' => $this->restaurantRepository->findAll(),
            ]);
        }

        return new Response('', 204);
    }
}
