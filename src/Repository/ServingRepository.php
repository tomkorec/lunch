<?php

namespace App\Repository;

use App\Entity\Meal;
use App\Entity\Restaurant;
use App\Entity\Serving;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Serving>
 *
 * @method Serving|null find($id, $lockMode = null, $lockVersion = null)
 * @method Serving|null findOneBy(array $criteria, array $orderBy = null)
 * @method Serving[]    findAll()
 * @method Serving[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Serving::class);
    }

    public function add(Serving $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Serving $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Serving[] Returns an array of Serving objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Serving
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
    /**
     * @throws NonUniqueResultException
     */
    public function findServingByMealAndDateAndRestaurant(Meal $meal, DateTime $date, Restaurant $restaurant): ?Serving
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.meal', 'm')
            ->leftJoin('s.restaurant', 'r')
            ->andWhere('m.id = :meal')
            ->andWhere('r.id = :restaurant')
            ->andWhere('s.date = :date')
            ->setParameter('meal', $meal->getId())
            ->setParameter('restaurant', $restaurant->getId())
            ->setParameter('date', $date)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findServingsForToday(Restaurant $restaurant = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('s');

        if ($restaurant) {
            $queryBuilder
                ->leftJoin('s.restaurant', 'r')
                ->andWhere('r = :restaurant')
                ->setParameter('restaurant', $restaurant);
        }

        return $queryBuilder
            ->andWhere('s.date = :date')
            ->setParameter('date', (new DateTime())->format('Y-m-d'));
    }
}
