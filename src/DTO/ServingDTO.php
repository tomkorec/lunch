<?php

namespace App\DTO;

class ServingDTO
{
    public string $name;
    public string $price;
    public array $sideDishes = [];
    public ?int $amount = null;
    public ?string $unit = null;
    public array $allergens = [];

}
