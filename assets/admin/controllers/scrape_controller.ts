import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
	private cardsTarget: HTMLElement;
	private iconTarget: HTMLElement;
	private buttonTarget: HTMLElement;
	private labelTarget: HTMLElement;

	private isLoading: boolean;

	static targets = ['icon', 'label', 'button', 'cards'];

	scrape() {
		if (this.isLoading) {
			return;
		}

		this.isLoading = true;

		this.rotateIcon(true);

		const formData = new FormData();
		formData.append('render_cards', '1');
		this.cardsTarget.classList.add('loading');

		fetch('/admin/scrape', {
			method: 'POST',
			body: formData
		}).then(response => {
			const status = response.status;
			this.isLoading = false;

			this.cardsTarget.classList.remove('loading');

			if (status === 200) {
				this.rotateIcon(false);
				response.text()
					.then(text => {
						this.cardsTarget.innerHTML = text;
					});
			}


			if (status !== 204 && status !== 200) {
				this.rotateIcon(false);
				this.iconTarget.classList.replace('fa-sync-alt', 'fa-exclamation-triangle');
				this.buttonTarget.classList.replace('btn-primary', 'btn-danger');
				this.labelTarget.textContent = 'Chyba';
				throw new Error(`Unexpected status code: ${status}`);
			}

			if (status === 204) {
				this.rotateIcon(false);

				this.iconTarget.classList.replace('fa-sync-alt', 'fa-check');
				this.buttonTarget.classList.replace('btn-primary', 'btn-success');
				this.labelTarget.textContent = 'Hotovo';
			}

		});
	}

	scrapeIndividual({ params: { code }, currentTarget }) {
		currentTarget.classList.remove('err');

		if (this.isLoading) {
			return;
		}

		this.isLoading = true;

		const formData = new FormData();
		formData.append('code', code);
		formData.append('render_cards', '1');
		this.cardsTarget.classList.add('loading');

		fetch('/admin/scrape', {
			method: 'POST',
			body: formData
		}).then(response => {
			const status = response.status;
			this.isLoading = false;

			this.cardsTarget.classList.remove('loading');

			if (status === 200) {
				this.rotateIcon(false);
				response.text()
					.then(text => {
						this.cardsTarget.innerHTML = text;
					});

			}

			if (status !== 204 && status !== 200) {
				currentTarget.classList.replace('fa-sync-alt', 'fa-exclamation-triangle');
				currentTarget.classList.add('err');
			}


		});

	}

	rotateIcon(status) {
		if (status) {
			this.iconTarget.classList.add('rotating');
		} else {
			this.iconTarget.classList.remove('rotating');
		}
	}
}
