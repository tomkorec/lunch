<?php

namespace spec\App\Entity;

use App\Entity\Meal;
use PhpSpec\ObjectBehavior;

class MealSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('name');
        $this->shouldHaveType(Meal::class);
    }

    function it_is_initializable_by_passing_parameters()
    {
        $this->beConstructedWith('name');
        $this->getName()->shouldReturn('name');
    }

    function it_has_a_name()
    {
        $this->beConstructedWith('Řízek s bramborovu kaší');
        $this->getName()->shouldReturn('Řízek s bramborovu kaší');
    }

    function it_has_a_vegetarian_flag()
    {
        $this->beConstructedWith('Řízek s bramborovu kaší');
        $this->setVegetarian(true);
        $this->isVegetarian()->shouldReturn(true);
    }

    function it_has_name_variants()
    {
        $this->beConstructedWith('Řízek s bramborovu kaší');
        $this->setNameVariants(['Řízek s bramborovu kaší', 'Řízek s bramborovou kaší']);
        $this->getNameVariants()->shouldReturn(['Řízek s bramborovu kaší', 'Řízek s bramborovou kaší']);
    }

    function it_returns_all_names_of_the_meal()
    {
        $this->beConstructedWith('Řízek s bramborovu kaší');
        $this->addNameVariant('Řízek se salátem');
        $this->getAllNames()->shouldReturn(['Řízek s bramborovu kaší', 'Řízek se salátem']);
    }

    function it_can_add_name_variant()
    {
        $this->beConstructedWith('Řízek s bramborovu kaší');
        $this->setNameVariants(['Řízek s bramborovu kaší', 'Řízek s bramborovou kaší a oblohou']);
        $this->addNameVariant('Řízek s bramborovou kaší a oblohou a kuřecím');
        $this->getNameVariants()->shouldReturn(['Řízek s bramborovu kaší', 'Řízek s bramborovou kaší a oblohou', 'Řízek s bramborovou kaší a oblohou a kuřecím']);
    }
    
}
