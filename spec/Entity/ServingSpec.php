<?php

namespace spec\App\Entity;

use App\Entity\Serving;
use PhpSpec\ObjectBehavior;

class ServingSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Serving::class);
    }

    function it_has_price()
    {
        $this->setPrice(1000);
        $this->getPrice()->shouldReturn(1000);
    }

    function it_returns_price_as_string()
    {
        $this->setPrice(100000);
        $this->getPriceAsString()->shouldReturn('1 000');
    }

    function it_returns_price_with_currency_as_string()
    {
        $this->setPrice(100000000);
        $this->getPriceAsString(true)->shouldReturn('1 000 000 Kč');
    }

    function it_returns_full_price()
    {
        $this->setPrice(100000);
        $this->getFullPrice()->shouldReturn(1000.0);
    }

    function it_can_have_no_price()
    {
        $this->setPrice(null);
        $this->getPrice()->shouldReturn(null);
    }

    function it_can_set_full_price_as_float()
    {
        $this->setFullPrice(100.0);
        $this->getPrice()->shouldReturn(10000);
    }

    function it_can_set_full_price_as_int()
    {
        $this->setFullPrice(100);
        $this->getPrice()->shouldReturn(10000);
    }


}
