<?php

namespace spec\App\Entity;

use App\Entity\Restaurant;
use PhpSpec\ObjectBehavior;

class RestaurantSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Restaurant::class);
    }

    function it_has_scrapeable_flag()
    {
        $this->setScrapeable(true);
        $this->isScrapeable()->shouldReturn(true);
    }

    function it_creates_code_from_name()
    {
        $this->setName('U Výčepu');
        $this->getCode()->shouldReturn('U_VYCEPU');
    }
}
