<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220821045830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE meal_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE meal ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE meal ADD CONSTRAINT FK_9EF68E9C12469DE2 FOREIGN KEY (category_id) REFERENCES meal_category (id)');
        $this->addSql('CREATE INDEX IDX_9EF68E9C12469DE2 ON meal (category_id)');

        //insert meal categories
        $this->addSql('INSERT INTO meal_category (id, name) VALUES (1, "Polévka")');
        $this->addSql('INSERT INTO meal_category (id, name) VALUES (2, "Hlavní chod")');
        $this->addSql('INSERT INTO meal_category (id, name) VALUES (3, "Dezert")');
        $this->addSql('INSERT INTO meal_category (id, name) VALUES (4, "Salát")');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meal DROP FOREIGN KEY FK_9EF68E9C12469DE2');
        $this->addSql('DROP TABLE meal_category');
        $this->addSql('DROP INDEX IDX_9EF68E9C12469DE2 ON meal');
        $this->addSql('ALTER TABLE meal DROP category_id');
    }
}
