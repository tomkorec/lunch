<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220807180255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE serving_side_dish (serving_id INT NOT NULL, side_dish_id INT NOT NULL, INDEX IDX_576B2FEABFC5A5DC (serving_id), INDEX IDX_576B2FEAC884D3E8 (side_dish_id), PRIMARY KEY(serving_id, side_dish_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE serving_side_dish ADD CONSTRAINT FK_576B2FEABFC5A5DC FOREIGN KEY (serving_id) REFERENCES serving (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serving_side_dish ADD CONSTRAINT FK_576B2FEAC884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serving ADD price INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE serving_side_dish DROP FOREIGN KEY FK_576B2FEABFC5A5DC');
        $this->addSql('ALTER TABLE serving_side_dish DROP FOREIGN KEY FK_576B2FEAC884D3E8');
        $this->addSql('DROP TABLE serving_side_dish');
        $this->addSql('ALTER TABLE serving DROP price');
    }
}
