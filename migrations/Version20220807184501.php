<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220807184501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE meal_restaurant (meal_id INT NOT NULL, restaurant_id INT NOT NULL, INDEX IDX_7CF9A4B5639666D6 (meal_id), INDEX IDX_7CF9A4B5B1E7706E (restaurant_id), PRIMARY KEY(meal_id, restaurant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meal_side_dish (meal_id INT NOT NULL, side_dish_id INT NOT NULL, INDEX IDX_7756AF84639666D6 (meal_id), INDEX IDX_7756AF84C884D3E8 (side_dish_id), PRIMARY KEY(meal_id, side_dish_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE meal_restaurant ADD CONSTRAINT FK_7CF9A4B5639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meal_restaurant ADD CONSTRAINT FK_7CF9A4B5B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meal_side_dish ADD CONSTRAINT FK_7756AF84639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meal_side_dish ADD CONSTRAINT FK_7756AF84C884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meal_restaurant DROP FOREIGN KEY FK_7CF9A4B5639666D6');
        $this->addSql('ALTER TABLE meal_restaurant DROP FOREIGN KEY FK_7CF9A4B5B1E7706E');
        $this->addSql('ALTER TABLE meal_side_dish DROP FOREIGN KEY FK_7756AF84639666D6');
        $this->addSql('ALTER TABLE meal_side_dish DROP FOREIGN KEY FK_7756AF84C884D3E8');
        $this->addSql('DROP TABLE meal_restaurant');
        $this->addSql('DROP TABLE meal_side_dish');
    }
}
